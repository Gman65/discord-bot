const Discord = require("discord.js")
const fs = require("fs")
const mysql = require('mysql');
const bot = new Discord.Client()
require('dotenv').config()
bot.commands = new Discord.Collection()
const prefix = "/"

// test
//const connection = mysql.createConnection(config)

bot.on("ready", () => {
    console.log("Ready as " + bot.user.tag + " With id " + bot.user.id)
    console.log(`https://discordapp.com/oauth2/authorize?client_id=${bot.user.id}&scope=bot&permissions=8`)
    if (fs.existsSync("./commands")) fs.readdir("./commands/", (err, files) => {

        if (err) console.log(err);
        let jsfile = files.filter(f => f.split(".").pop() === "js");
        if (jsfile.length <= 0) {
            console.log("Could not find any commands.");
            return;
        }

        jsfile.forEach((f, i) => {
            let props = require(`./commands/${f}`);
            console.log(`Command ${f.split(".js")[0]} loaded.`);
            bot.commands.set(props.help.name, props);
        });
    });
});

bot.on("message", message => {
    if (message.guild === null) return;
    const swearWords = ["darn", "shucks", "frak", "shite"];
    if (swearWords.some(word => message.content.includes(word))) {
        message.reply("Oh no you said a bad word!!!");
        // Or just do message.delete();
    }
    if (message.mentions.members.first()) {
        let myRole = message.guild.roles.find(role => role.name === "*");
        if (message.author.bot) return;
        let author = message.author.id;
        if (message.member.roles.has(myRole.id)) return;
        message.delete();
        if (message.mentions.members.first().roles.has(myRole.id)) message.channel.send(`<@${author}> just mentioned staff ` + "¯\\_(ツ)_/¯");
    }
});


bot.on("message", message => {
    //message.channel.send(`Debug ${message}`);
    if (!message.content.startsWith(prefix)) return
    let array = message.content.split(" ")
    let args = array.splice(1)
    let cmd = array[0].slice(1)
    let commandfile = bot.commands.get(cmd);
    if (commandfile) {
        message.delete();
        try {
            commandfile.run(bot, message, args);
        } catch(e) {
            message.channel.send(`An error accured while performing command ${cmd}: ${e}`);
            console.error(e);
        }
    }
})

let BOT_TOKEN = process.env.BOT_TOKEN
bot.login(BOT_TOKEN)
